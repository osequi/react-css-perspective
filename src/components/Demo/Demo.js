import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import ReactMd from "react-md-file";

/**
 * Imports other components and hooks
 */
import SquareMove from "../SquareMove";
import SquareRotate from "../SquareRotate";
import { Headings } from "@bit/osequi.test.react-semantic-elements";

/**
 * Defines the prop types
 */
const propTypes = {};

/**
 * Defines the default props
 */
const defaultProps = {};

/**
 * Defines the styles
 */
const useStyles = makeStyles((theme) => ({
  container: {
    overflowX: "hidden",
  },
}));

/**
 * Displays the component
 */
const Demo = (props) => {
  const { container } = useStyles(props);

  return (
    <div className={clsx("Demo", container)}>
      <ReactMd fileName="./Demo.md" />
      <SquareMove />
      <SquareRotate />
    </div>
  );
};

Demo.propTypes = propTypes;
Demo.defaultProps = defaultProps;

export default Demo;
export { propTypes as DemoPropTypes, defaultProps as DemoDefaultProps };
