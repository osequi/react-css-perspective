## Rotation

When rotate on X and Y axis, perspective has influence on the object.

It makes the rotation natural. It makes the rotation visible also around the Z-axis giving to the motion a real sense.

Perspective value, again, can make the movement exaggerated (when too close) or almost invisible (when too far).
